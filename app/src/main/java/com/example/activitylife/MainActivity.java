package com.example.activitylife;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

/**
 * Класс предназначен для демонстрации жизненного цикла Activity и создания меню а ActionBar'e
 */
public class MainActivity extends AppCompatActivity {

    public static final String TAG_START = "StartActivity";
    private static final String TAG = "counter";
    private int counter = 1337;

    /* Вызывается при создании Активности */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Инициализация Активности */
        setContentView(R.layout.activity_main);
        Log.d(TAG_START, "hello from onCreate");
        Toast.makeText(this, "hello from onCreate() | counter: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Вызывается перед выходом из активного состояния, позволяя сохранить состояние в объекте savedInstanceState */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        /* Объект savedInstanceState будет в последующем передан методам onCreate и onRestoreInstanceState*/
        savedInstanceState.putInt(TAG, counter);
        Log.d(TAG_START, "hello from onSaveInstanceState");
        Toast.makeText(this, "hello from onSaveInstanceState() | save: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Вызывается после завершения метода onCreate. Используется для восстановления состояния UI */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        /* Восстановление состояние UI из объекта savedInstanceState. Данный объект также был передан методу onCreate*/
        counter = savedInstanceState.getInt(TAG, counter);
        Log.d(TAG_START, "hello from onRestoreInstanceState");
        Toast.makeText(this, " hello from onRestoreInstanceState() | restore: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Вызывается перед тем, как Активность снова становится видимой */
    @Override
    public void onRestart(){
        super.onRestart();
        /* Восстановить состояние UI с учетом того, что данная Активность уже была видимой */
        Log.d(TAG_START, "hello from onRestart");
        Toast.makeText(this, "hello from onRestart() | counter: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Вызывается, когда Активность стала видимой */
    @Override
    public void onStart(){
        super.onStart();
        /* Проделать необходимые действия для Активности, видимой на экране */
        Log.d(TAG_START, "hello from onStart");
        Toast.makeText(this, "hello from onStart | counter: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Должен вызываться в начале видимого состояния. На самом деле Android вызывает данный обработчик только для Активностей, восстановленных из неактивного состояния */
    @Override
    public void onResume(){
        super.onResume();
        /* Восстановить приостановленные обновления UI, потоки и процессы, замороженные, когда Активность была в неактивном состоянии */
        Log.d(TAG_START, "hello from onResume");
        Toast.makeText(this, "hello from onResume() | counter: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Вызывается перед выходом из активного состояния */
    @Override
    public void onPause(){
        super.onPause();
        /* «Заморозить» обновления UI, потоки или «трудоемкие» процессы, не нужные, когда Активность не на переднем плане*/
        Log.d(TAG_START, "hello from onPause");
        Toast.makeText(this, "hello from onPause() | counter: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Вызывается перед выходом из видимого состояния */
    @Override
    public void onStop(){
        super.onStop();
        /* «Заморозить» обновления UI, потоки или «трудоемкие» процессы, не нужные, когда Активность не на переднем плане. Сохранить все данные и изменения в UI, так как процесс может быть в любой момент убит системой */
        Log.d(TAG_START, "hello from onStop");
        Toast.makeText(this, "hello from onStop() | counter: " + counter, Toast.LENGTH_LONG).show();
    }

    /* Вызывается перед уничтожением активности */
    @Override
    public void onDestroy(){
        super.onDestroy();
        /* Освободить все ресурсы, включая работающие потоки, соединения с БД и т. д.*/
        Log.d(TAG_START, "hello from onDestroy");
        Toast.makeText(this, "hello from onDestroy() | counter: " + counter, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* Добавить пункты в панель действий, если она присутствует */
        getMenuInflater().inflate(R.menu.main, menu);
        Log.d(TAG_START, "hello from onCreateOptionsMenu");
        Toast.makeText(this, "hello from onCreateOptionsMenu | add item1, item2, item3 ", Toast.LENGTH_LONG).show();
        return true;
    }
}